import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // 00000001 -> 1
        // 00000010 -> 2 -> 1 * 2^1 + 0 * 2^0
        // 00000011 -> 3 -> 1 * 2^1 + 1 * 2^0

        // typ nazwa;
        // typ nazwa = wartosc;
/*
        byte mojBajt = 10;
        int mojaLiczba = 10;
        double mojaLiczbaZmiennoprzecinkowa = 2.5;
        float mojFloat = 2.3f;
        short mojShort = 123;

        boolean prawda = true;
        char litera = 'ż';
        String zdanie = "Ala ma kota";


        System.out.println(mojaLiczba);
        System.out.println(mojaLiczbaZmiennoprzecinkowa);
        System.out.println(prawda);
        System.out.println(litera);
        System.out.println(zdanie);


        // operacje na zmiennych
        System.out.println("\nOperacje na zmiennych");
        int a = 10;
        int b = 20;

        int c = a + b;
        int d = a - b;
        int e = a * b;
        double f = 10.0 / 20.0;

        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);

        System.out.println(2 * 100);

        String osoba = "Jan" + " " + "Kowalski";
        System.out.println(osoba);

        System.out.println("Reszta z dzielenia przez 2: " + (10 % 2));
        System.out.println("Reszta z dzielenie przez 2: " + (11 % 2));


        System.out.println("\nOperatory logiczne");
        // OR, AND, XOR, NOT
        System.out.println("true || true = " + (true || true));
        System.out.println("true || false = " + (true || false));
        System.out.println("false || true = " + (true || false));
        System.out.println("false || false = " + (false || false));


        System.out.println("true && true = " + (true && true));
        System.out.println("true && false = " + (true && false));
        System.out.println("false && true = " + (true && false));
        System.out.println("false && false = " + (false && false));

        System.out.println("true ^ true = " + (true ^ true));
        System.out.println("true ^ false = " + (true ^ false));
        System.out.println("false ^ true = " + (true ^ false));
        System.out.println("false ^ false = " + (false ^ false));

        System.out.println("(true || false) && true = " + ((true || false) && true));
        System.out.println("!true " + !true); // -> false
        System.out.println("!false " + !false); // -> true


        System.out.println("\nPorownania");
        System.out.println(10 > 5);
        System.out.println(10 >= 5);
        System.out.println(10 != 5);
        System.out.println(10 < 5);
        System.out.println(10 <= 5);


        // .....|0.........30|.....
        int x = -10;
        System.out.println(!((x > 0) && (x < 30)));





*/

// the beginning
        Scanner scanner = new Scanner(System.in);
        int decimalNumber;
        int base;
        int N;
        String result="";

        System.out.print("Type the number in decimal: ");
        decimalNumber = scanner.nextInt();

        System.out.print("Type the system base for recalculation: ");
        base = scanner.nextInt();
        N = decimalNumber;
        do{
            result = result + String.valueOf(N % base);  // result po każdej iteracji dostaje dopisane skonwertowane
            //na String wartość operacji N module base.
            //nie można dodać do typu String wartości typu Int, kompilator nie potrafi tego, nie wie o co CI chodzi

            N = N/base;
        } while( N > 0 );
        // po skończonej pętli musimy odwrócić ciąg zero-jedynkowy trzymany pod result. Nie pamiętam dokładnie dlaczego
        // wydawało by się, że nie trzeba bo rozkładasz liczbę po kolei dzieląc na dwa..
        String reverseResult = new StringBuffer(result).reverse().toString();
        // to powyższe wygooglowałem, do zapamiętania albo googlowania następnym razem.
        // easy: deklaracja reverseResult i przypisanie mu nowego obiektu klasy StringBuffer i zainicjowanie tego wartością
        // result, wywołanie na tym metody reverse() i konwersja ze StringBuffera na String. Zauważ, że ta klasa StringBuffer
        // nie jest importowana, czyli jest standardową klasą w przestrzeni nazw Javy. Z tą konwersją to chodzi o to, że
        // wrzucasz ciąg znaków do obiekty StringBuffer który ma napisane różne funkcje operowania na stringach, a jak
        // uzyskasz to co chcesz to nie możesz poprostu przypisać = do String reverseResult bo Java nie potrafi porównywać
        // różnych typów, musi mieć te same typy danych, int=int, String=String, .... dlatego taki kombajn do obsługi na
        // Stringach jak StringBuffer ma metodę konwersji na typ String. Jeśli to jest niezrozumiałe to powiedz, wytłumaczę
        // Ci bardziej bo to kluczowe dla programowania.


        System.out.println("Warotość liczby " + String.valueOf(decimalNumber) +
                " w systemie o podstawie: " + String.valueOf(base) + "wynosi: " + reverseResult);

        System.exit(0);
    }
}
