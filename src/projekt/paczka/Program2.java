package projekt.paczka;

import java.util.Scanner;

public class Program2 {
    public static void main(String[] args) {
        // instrukcja warunkowa
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj temperature: ");

        double temperatura = scanner.nextDouble();

        System.out.println("Podales temperature = " + temperatura);

        if(temperatura > 30.0) {
            System.out.println("Jest goarco");
        } else if(temperatura > 20.0) {
            System.out.println("Jest cieplo");
        } else {
            System.out.println("Jest zimno");
        }



        System.out.println("Poza if..else");


    }
}
