package paczka;

public class Main {
    public static void main(String[] args) {
        // 00000001 -> 1
        // 00000010 -> 2 -> 1 * 2^1 + 0 * 2^0
        // 00000011 -> 3 -> 1 * 2^1 + 1 * 2^0

        // typ nazwa;
        // typ nazwa = wartosc;

        byte mojBajt = 10;
        int mojaLiczba = 10;
        double mojaLiczbaZmiennoprzecinkowa = 2.5;
        float mojFloat = 2.3f;
        short mojShort = 123;

        boolean prawda = true;
        char litera = 'ż';
        String zdanie = "Ala ma kota";


        System.out.println(mojaLiczba);
        System.out.println(mojaLiczbaZmiennoprzecinkowa);
        System.out.println(prawda);
        System.out.println(litera);
        System.out.println(zdanie);


        // operacje na zmiennych
        System.out.println("\nOperacje na zmiennych");
        int a = 10;
        int b = 20;

        int c = a + b;
        int d = a - b;
        int e = a * b;
        double f = 10.0 / 20.0;

        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);

        System.out.println(2 * 100);

        String osoba = "Jan" + " " + "Kowalski";
        System.out.println(osoba);

        System.out.println("Reszta z dzielenia przez 2: " + (10 % 2));
        System.out.println("Reszta z dzielenie przez 2: " + (11 % 2));


        System.out.println("\nOperatory logiczne");
        // OR, AND, XOR, NOT
        System.out.println("true || true = " + (true || true));
        System.out.println("true || false = " + (true || false));
        System.out.println("false || true = " + (true || false));
        System.out.println("false || false = " + (false || false));


        System.out.println("true && true = " + (true && true));
        System.out.println("true && false = " + (true && false));
        System.out.println("false && true = " + (true && false));
        System.out.println("false && false = " + (false && false));

        System.out.println("true ^ true = " + (true ^ true));
        System.out.println("true ^ false = " + (true ^ false));
        System.out.println("false ^ true = " + (true ^ false));
        System.out.println("false ^ false = " + (false ^ false));

        System.out.println("(true || false) && true = " + ((true || false) && true));
        System.out.println("!true " + !true); // -> false
        System.out.println("!false " + !false); // -> true


        System.out.println("\nPorownania");
        System.out.println(10 > 5);
        System.out.println(10 >= 5);
        System.out.println(10 != 5);
        System.out.println(10 < 5);
        System.out.println(10 <= 5);


        // .....|0.........30|.....
        int x = -10;
        System.out.println(!((x > 0) && (x < 30)));






    }
}
