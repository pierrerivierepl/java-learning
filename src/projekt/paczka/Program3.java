package projekt.paczka;

import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        //instrukcja wyboru

        System.out.println("1 - Opcja 1");
        System.out.println("2 - Opcja 2");
        System.out.println("3 - Opcja 3");
        System.out.print("Podaj wybor: ");


        Scanner scanner = new Scanner(System.in);

        int wybor = scanner.nextInt();

        switch (wybor) {
            case 1:
                System.out.println("Wybrano opcje 1");
                break;
            case 2:
                System.out.println("Wybrano opcje 2");
                break;
            case 3:
                System.out.println("Wybrano opcje 3");
                break;

            default:
                System.out.println("Nie ma takiej opcji");
        }


        System.out.println("Tutaj jeszcze program dziala...");
    }
}
